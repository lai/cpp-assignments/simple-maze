# unittests

```bash
AOR # Arithmetic Operator Replacement: x * y -> x + y
ROR # Relational Operator Replacement: x < y -> x >= y
CVR # Constant Value Replacement: sizeof(int) -> sizeof(double)
LCR # Logical Connector Replacement: A && B -> A || B
LOR # Logical Operator Replacement: A & B -> A | B
UOD # Unary Operator Deletion: while (! thing) -> while (thing)
UOI # Unary Operator Insertion: while (thing) -> while (! thing)
```
