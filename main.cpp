#include "maze.h"
// driver program to test above function
int main()
{
    int maze[N][N] = { { 1, 0, 0, 0 },
                       { 1, 1, 0, 1 },
                       { 0, 1, 0, 0 },
                       { 1, 1, 1, 1 } };
 
    solveMaze(maze);
    return 0;
}
