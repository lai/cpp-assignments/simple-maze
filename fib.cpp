#include "fib.h"

int fib1(int n)
{
    if (n <= 1)
        return n;
    return fib1(n - 2) + fib1(n - 1);
}


static int _fib(int n)
{
    if (n <= 1)
        return n;
    return _fib(n - 2) + _fib(n - 1);
}


int fib2(int n)
{
        return _fib(n);
}
